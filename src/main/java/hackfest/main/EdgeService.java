package hackfest.main;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.reactive.messaging.Channel;
import org.eclipse.microprofile.reactive.messaging.Emitter;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.jboss.logging.Logger;

import hackfest.models.Gas;
import hackfest.models.Pollution;
import io.quarkus.scheduler.Scheduled;

@ApplicationScoped
public class EdgeService {

  private static final Logger LOGGER = Logger.getLogger("EdgeService");

  @Inject
  @RestClient
  SensorApdapterService sensorApdapterService;

  @Inject
  @Channel("getGas")
  Emitter<String> gasEmitter;

  @Inject
  @Channel("getPollution")
  Emitter<String> pollutionEmitter;

  @Scheduled(every = "{schedule}")
  public void sendGas() {
    Gas gas;
    String gasString;
    try {
      LOGGER.info("Get Gas from python container");
      gas = sensorApdapterService.getGas();
      gas.setInstant();
      gasString = gas.getGasString();
      gasEmitter.send(gasString).whenComplete((unused, throwable) -> LOGGER.info("Managed to send data."))
          .exceptionally(throwable -> {
            LOGGER.error(throwable);
            return null;
          });
    } catch (Exception e) {
      LOGGER.error(e);
    }
  }

  @Scheduled(every = "{schedule}")
  public void sendPollution() {
    Pollution pollution;
    String pollutionString;
    try {
      LOGGER.info("Get Pollution from python container");
      pollution = sensorApdapterService.getPollution();
      pollution.setInstant();
      pollutionString = pollution.getPollutionString();
      pollutionEmitter.send(pollutionString).whenComplete((unused, throwable) -> LOGGER.info("Managed to send data."))
          .exceptionally(throwable -> {
            LOGGER.error(throwable);
            return null;
          });
    } catch (Exception e) {
      LOGGER.error(e);
    }
  }
}
