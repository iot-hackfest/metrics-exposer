package hackfest.main;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.jboss.logging.Logger;

import hackfest.models.CoordinatesBean;
import hackfest.models.StationId;
import io.quarkus.runtime.ShutdownEvent;
import io.quarkus.runtime.StartupEvent;

@ApplicationScoped
public class AppLifecycleBean {
  private static final Logger LOGGER = Logger.getLogger("AppLifecycleBean");

  @Inject
  @RestClient
  SensorApdapterService sensorApdapterService;

  @Inject
  @RestClient
  DataHubClientService dataHubClientService;

  String id;

  @ConfigProperty(name = "serialNumber")
  String serial;

  @ConfigProperty(name = "name")
  String name;

  @ConfigProperty(name = "address")
  String address;

  double longitude;
  double latitude;

  void onStart(@Observes StartupEvent ev) throws Exception {
    LOGGER.info(id);
    // try {
    // LOGGER.info("Get serial");
    // serial = sensorApdapterService.getSerialNumber();
    // LOGGER.info("Serial: " + serial);
    // } catch (Exception e) {
    // LOGGER.error("Can't fetch serial, Error: " + e);
    // serial = "0000";
    // }

    // Get Coordinates (longitude, latitude)
    CoordinatesBean coordinates = new CoordinatesBean(); 
    coordinates.getCoordinates(address);
    longitude = coordinates.getLongitude();
    latitude = coordinates.getLatitude();
    LOGGER.info("Longitude:" + longitude + "Latitude:" + latitude);

    // register/serial/{serial}/name/{name}/longitude/{longitude}/latitude/{latitude}
    try {
      LOGGER.info("Get Id from DataHub");
      id = dataHubClientService.register(serial, name, longitude, latitude);
      StationId.setStationId(Integer.parseInt(id));
      LOGGER.info("DataHub id: " + id);
    } catch (Exception e) {
      LOGGER.error("Can't fetch from DataHub. Error: " + e);
    }
  }

  void onStop(@Observes ShutdownEvent ev) {

    try {
      LOGGER.info("unregister id: " + id);
      dataHubClientService.unregister(Integer.parseInt(id));
    } catch (NumberFormatException e) {
      LOGGER.error("NumberFormatException" + e);
    } catch (Exception e) {
      LOGGER.error(e);
    }

  }
}
