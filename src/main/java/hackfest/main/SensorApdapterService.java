package hackfest.main;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import hackfest.models.Gas;
import hackfest.models.Pollution;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

@RegisterRestClient(baseUri = "http://10.89.0.1:5000/")
public interface SensorApdapterService {

  @GET
  @Path("/getSerialNumber")
  @Produces(MediaType.TEXT_PLAIN)
  public String getSerialNumber();

  @GET
  @Path("/getGas")
  @Produces(MediaType.APPLICATION_JSON)
  public Gas getGas();

  @GET
  @Path("/getPollution")
  @Produces(MediaType.APPLICATION_JSON)
  public Pollution getPollution();
}
