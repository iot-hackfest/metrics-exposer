package hackfest.models;

import java.time.Instant;

import io.vertx.core.json.JsonObject;

public class Pollution {
  public int PM1_0;
  public int PM2_5;
  public int PM10;
  public int PM1_0_atm;
  public int PM2_5_atm;
  public int PM10_atm;
  public int gt0_3um;
  public int gt0_5um;
  public int gt1_0um;
  public int gt2_5um;
  public int gt5_0um;
  public int gt10um;

  public int stationId = StationId.stationId;
  public String instant;

  public Pollution() {

  }

  public Pollution(int PM1_0, int PM2_5, int PM10, int PM1_0_atm, int PM2_5_atm, int PM10_atm, int gt0_3um, int gt0_5um,
      int gt1_0um, int gt2_5um, int gt5_0um, int gt10um) {
    this.PM1_0 = PM1_0;
    this.PM2_5 = PM2_5;
    this.PM10 = PM10;
    this.PM1_0_atm = PM1_0_atm;
    this.PM2_5_atm = PM2_5_atm;
    this.PM10_atm = PM10_atm;
    this.gt0_3um = gt0_3um;
    this.gt0_5um = gt0_5um;
    this.gt1_0um = gt1_0um;
    this.gt2_5um = gt2_5um;
    this.gt10um = gt10um;
  }

  public void setInstant() {
    instant = Instant.now().toString();
  }

  public String getPollutionString() {
    return new JsonObject().put("stationId", stationId).put("instant", instant).put("PM1_0", PM1_0).put("PM2_5", PM2_5)
        .put("PM10", PM10).put("PM1_0_atm", PM1_0_atm).put("PM2_5_atm", PM2_5_atm).put("PM10_atm", PM10_atm)
        .put("gt0_3um", gt0_3um).put("gt0_5um", gt0_5um).put("gt1_0um", gt1_0um).put("gt2_5um", gt2_5um)
        .put("gt10um", gt10um).toString();
  }
}
