package hackfest.models;

import javax.enterprise.context.ApplicationScoped;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;

import org.jboss.logging.Logger;

import hackfest.models.CoordinatesBean;

@ApplicationScoped
public class CoordinatesBean {

  private static final Logger LOGGER = Logger.getLogger("CoordinatesBean");

  double longitude;
  double latitude;

  public CoordinatesBean(final double longitude, final double latitude) {
    this.longitude = longitude;
    this.latitude = latitude;
  }

  public CoordinatesBean() {
  }

  public void setLatitude(final double latitude) {
    this.latitude = latitude;
  }

  public void setLongitude(final double longitude) {
    this.longitude = longitude;
  }

  public double getLatitude() {
    return latitude;
  }

  public double getLongitude() {
    return longitude;
  }

  private static String encodeValue(String value) {
    try {
        return URLEncoder.encode(value, StandardCharsets.UTF_8.toString());
    } catch (UnsupportedEncodingException ex) {
        throw new RuntimeException(ex.getCause());
    }
  } 

  public CoordinatesBean getCoordinates(String address) throws Exception {

    StringBuffer query = null;
    query = new StringBuffer();
    query.append("https://nominatim.openstreetmap.org/search?q=");

    query.append(encodeValue(address));

    query.append("&format=json&addressdetails=1");
    LOGGER.debug("Query:" + query);
    URL url = new URL(query.toString());

    try (InputStream is = url.openStream();
      
      JsonReader reader = Json.createReader(is)) {
      JsonArray jsonArray = reader.readArray();
      LOGGER.debug(jsonArray.toString());
      JsonObject jsonObject = jsonArray.getJsonObject(0);
      LOGGER.debug(jsonObject.toString());
      setLongitude(Double.parseDouble(jsonObject.getString("lon")));
      setLatitude(Double.parseDouble(jsonObject.getString("lat")));
    }
    return this;
  }
}