package hackfest.models;

import java.time.Instant;

import io.vertx.core.json.JsonObject;

public class Gas {
  public Double adc;
  public double reducing;
  public double nh3;
  public double oxidising;

  public int stationId = StationId.stationId;
  public String instant;

  public Gas() {
  }

  public Gas(Double adc, double reducing, double nh3, double oxidising) {
    this.adc = adc;
    this.reducing = reducing;
    this.nh3 = nh3;
    this.oxidising = oxidising;
  }

  public Double getAdc() {
    return adc;
  }

  public double getReducing() {
    return reducing;
  }

  public double getNh3() {
    return nh3;
  }

  public double getOxidising() {
    return oxidising;
  }

  public void setInstant() {
    instant = Instant.now().toString();
  }

  public String getGasString() {
    if(adc == null){
      adc = 0.0;
    }

    return new JsonObject().put("stationId", stationId).put("instant", instant).put("adc", adc)
        .put("reducing", reducing).put("nh3", nh3).put("oxidising", oxidising).toString();
  }
}
