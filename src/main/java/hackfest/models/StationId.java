package hackfest.models;

public class StationId {
  public static int stationId;

  public static void setStationId(int stationId) {
    StationId.stationId = stationId;
  }

  public static int getStationId() {
    return stationId;
  }
}
