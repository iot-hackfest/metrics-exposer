package hackfest.converter;

import javax.enterprise.context.ApplicationScoped;

import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.eclipse.microprofile.reactive.messaging.Outgoing;
import org.jboss.logging.Logger;

import io.smallrye.reactive.messaging.annotations.Broadcast;

@ApplicationScoped
public class Converter {

  private static final Logger LOGGER = Logger.getLogger("Converter");

  @Incoming("getGas")
  @Outgoing("gas")
  @Broadcast
  public String sendGas(String word) {
    LOGGER.info("Send Gas to the Hub: " + word);
    return word;
  }

  @Incoming("getPollution")
  @Outgoing("pollution")
  @Broadcast
  public String sendPollution(String pollution) {
    LOGGER.info("Send Pollution to the Hub: " + pollution);
    return pollution;
  }
}
